#!/usr/bin/perl

# yawsproxy
# Copyright (C) 2014 Hideki EIRAKU

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

use Socket;
use Digest::SHA qw (sha1_base64);

binmode (STDIN);
binmode (STDOUT);
$| = 1;
die if !setsockopt (STDOUT, Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1);

# Read request
$buf = "";
$i = 0;
while ($len = sysread (STDIN, $buf, 100, $i)) {
    $i += $len;
    last if (index ($buf, "\r\n\r\n") != -1);
}

@requests = split (/\r\n/, $buf);
$HTTP_SEC_WEBSOCKET_KEY = "";
$host = "";
$service = "";
foreach my $line (@requests) {
    if ($line =~ /^Sec-WebSocket-Key:/) {
	$HTTP_SEC_WEBSOCKET_KEY = $line;
	$HTTP_SEC_WEBSOCKET_KEY =~ s/^Sec-WebSocket-Key: *//;
	$HTTP_SEC_WEBSOCKET_KEY =~ s/ *$//;
    } elsif ($line =~ /^GET \//) {
	(undef, undef, $host, $service) = split (/[ \/]/, $line);
    }
}
if ($#ARGV >= 1) {
    $host = $ARGV[0];
    $service = $ARGV[1];
}

if ($HTTP_SEC_WEBSOCKET_KEY eq "" || # It is not a Websocket request.
    $host eq "" || $service eq "") { # No host or service specified.
    print "HTTP/1.1 403 Forbidden\r\n";
    print "Connection: close\r\n";
    print "Content-Type: text/html; charset=US-ASCII\r\n";
    print "\r\n";
    print "<!DOCTYPE html>\r\n";
    print "<title>403 Forbidden</title><h1>403 Forbidden</h1>\r\n";
    exit (0);
}

$websocket_uuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
$SEC_WEBSOCKET_ACCEPT =
    sha1_base64 ($HTTP_SEC_WEBSOCKET_KEY.$websocket_uuid)."=";

$reply = "";
$reply .= "HTTP/1.1 101 Switching Protocols\r\n";
$reply .= "Upgrade: websocket\r\n";
$reply .= "Connection: Upgrade\r\n";
$reply .= "X-Sec-WebSocket-Key: $HTTP_SEC_WEBSOCKET_KEY\r\n"; # for debugging
$reply .= "Sec-WebSocket-Accept: $SEC_WEBSOCKET_ACCEPT\r\n";
$reply .= "Sec-WebSocket-Protocol: binary\r\n";
$reply .= "\r\n";
syswrite (STDOUT, $reply, length ($reply));

%hints = (socktype => SOCK_STREAM);
($err, @res) = Socket::getaddrinfo ($host, $service, \%hints);
die "getaddrinfo failed: $err" if $err;
$err = 1;
foreach my $ai (@res) {
    socket (S, $ai->{family}, $ai->{socktype}, $ai->{protocol}) or next;
    connect (S, $ai->{addr}) or next;
    $err = 0;
    last;
}
die if $err;
binmode (S);
die if !setsockopt (S, Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1);

if (fork ()) {				# parent process
	&sub_read_from_client ();
	shutdown (S, 1);
	close (S);
	exit (0);
} else {				# child process
	&sub_read_from_server ();
	shutdown (STDOUT, 1);
	close (S);
	exit (0);
}
# not reached

sub sub_read_from_client {
    my ($buf, $len, @mask, $partlen, @part, $i, $m);

    while (sysread (STDIN, $buf, 1) == 1) {
	last if ($buf ne "\x82");
	last if sysread (STDIN, $buf, 1) != 1;
	$len = unpack ("C", $buf);
	last if ($len < 0x80);
	$len -= 0x80;
	if ($len == 0x7e) {
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len = unpack ("C", $buf) * 0x100;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf);
	} elsif ($len == 0x7f) {
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len = unpack ("C", $buf) * 0x100000000000000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x1000000000000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x10000000000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x100000000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x1000000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x10000;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf) * 0x100;
	    last if sysread (STDIN, $buf, 1) != 1;
	    $len += unpack ("C", $buf);
	}
	last if sysread (STDIN, $buf, 1) != 1;
	$mask[0] = unpack ("C", $buf);
	last if sysread (STDIN, $buf, 1) != 1;
	$mask[1] = unpack ("C", $buf);
	last if sysread (STDIN, $buf, 1) != 1;
	$mask[2] = unpack ("C", $buf);
	last if sysread (STDIN, $buf, 1) != 1;
	$mask[3] = unpack ("C", $buf);
	$m = 0;
	while ($len > 0 &&
	       ($partlen = sysread (STDIN, $buf, $len > 4096 ? 4096 : $len))) {
	    @part = unpack ("C*", $buf);
	    for ($i = 0; $i < $partlen; $i++) {
		$part[$i] ^= $mask[$m];
		$m = ($m + 1) & 3;
	    }
	    syswrite (S, pack ("C*", @part), $partlen);
	    $len -= $partlen;
	}
	last if ($len > 0);
    }
}

sub sub_read_from_server {
    my ($len, $buf);

    while ($len = sysread (S, $buf, 0xffff, 4)) {
	if ($len < 0x7e) {
	    substr ($buf, 2, 2, "\x82".pack ("C", $len));
	    syswrite (STDOUT, $buf, $len + 2, 2);
	} else {
	    substr ($buf, 0, 4,
		    "\x82\x7e".pack ("CC", $len >> 8, $len & 0xff));
	    syswrite (STDOUT, $buf, $len + 4);
	}
    }
    syswrite (STDOUT, "\x88\x05\x10\xe1foo", 7);
}
